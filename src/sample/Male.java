package sample;

public class Male extends Character {

  public Male(String name, int age) {
    this.name = name;
    this.age = age;
    this.fatigue = 3;
    this.hunger = 2;
    this.thirst = 2;
    this.inventory = new Item[8];
  }

  @Override
  public void increaseTiredness(){
    /* Male character starts off more tired, hungry, and thirsty, but becomes less tired compared to his female partener
     as the inv. becomes heavier */
    double tirednessFactor = ((double)invSize() +5) /8;
    this.fatigue += 0.3 * tirednessFactor;
    this.hunger += 0.3 * tirednessFactor;
    this.thirst += 0.4;
  }

  @Override
  public boolean checkTiredness() {
    if((this.fatigue > 10) || (this.hunger > 10) || (this.thirst > 10)) {
      return false;
    }
    return true;
  }

}
