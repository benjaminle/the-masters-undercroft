package sample;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Story {
  static Scanner scanner = new Scanner(System.in);
  static Character player;

  static Item fishingPole;
  static Item waterBottle;
  static Item brokenGlasses;
  static Item jamesTears;
  static Item luckyPotion;
  static Item hadesRing;
  static Item mapThrForbiddenWood;
  static Item pencil;
  static Item openDiary;

  public Story() {
    try {
      fillPlayerQuestion();
      fillDoctorResponse();

      //Create items to be used discovered and used through out the story

      fishingPole = new Item("Fishing pole", "A useful tool on a deserted island, or so you think");
      waterBottle = new Item("Water Bottle", "There's water inside");
      brokenGlasses = new Item("Broken pair of glasses", "One of the eyehole is shattered, but the\n"
          + "other is in good condition. When looking through, \n"
          + "you seem to be trasnported to a different world.\n");
      jamesTears = new Item("James Potter's tears",
          "His last gift on Earth will be your only hope");
      luckyPotion = new Item("Sev's Lucky Potion", "Use it once and use it wisely");
      hadesRing = new Item("Hades's Ring", "The gates between worlds converge upon this artifact");
      mapThrForbiddenWood = new Item("Map of the Forbidden Woods", "      ----**()**----\n"
          + "     ---**-----*---\n"
          + "     -----***--*---\n"
          + "     ---**-----***-\n"
          + "     -----***--*---\n"
          + "     00----0-0-0---0\n");
      pencil = new Item("Pencil", "Good for taking notes");
      openDiary = new Item("Master's diary",
          "     April 19th, 1984 \n Tomorrow, I will end my life - my torture chamber. \n"
              + "People of this town has turned vile and hateful against one another. They eat each other's heart to gain a few pennies \n"
              + "in their pockets. It is this greed that will kill them, sooner or later.  But tomorrow, I will cut these strings on me. \n"
              + "Tomorrow, my life will be complete. And they will learn to love or die. \n"
              + "      Himnus Kradle    \n"); //MD: diary of under-croft's master

      player = createCharacter();
      System.out.println("The story begins...\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("You: Oh, dear... what happened to me? I was preparing myself for...\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You feel a sudden pain in your neck and a chill that creeps down the back of your neck. \n"
          + "Light wind pierces through the cotton of your shirt. Last time you remembered, you were preparing your \n"
          + "bed for the night before a storm hits and drags your boat down to deep water. Waves of water drowned your \n"
          + "face, and you passed out on a plank you managed to hold onto.\n");
      TimeUnit.SECONDS.sleep(15);

      System.out.println("A: walk down the beach");
      System.out.println("B: search the remains of your boat \n");

      beachMove(); //Imbedded in beach move is roomMove();

      TimeUnit.SECONDS.sleep(3);
      System.out.println("For hours, you walk under the sodden clouds. They look heavy, but they aren't willing to \n"
          + "shed any water. The rhythmic splash of water carries your mind back home...\n");
      TimeUnit.SECONDS.sleep(6);
      System.out.println("     MEMORY OF HOME     \n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("You: Mom! Can we have ratatouille today?\n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("Mom: Oh, " + player.name + ". You know the country is in tough times right now. I heard \n"
          + "     the Allies are storming into Berlin this week. Maybe one day, we can have ratatouille for you okay dear?\n");
      TimeUnit.SECONDS.sleep(6);
      System.out.println("    For all of your life, you've wished to run away from war, poverty, and the incessant worry \n"
          + "for tomorrow. And that's why you came on this journey in the first place. \n"
          + "On a train to work, you've heard of a buried treasure on the coast of Scotland, but after wandering the \n"
          + "seas for three months, you hadn't found any new hint to it.\n");
      TimeUnit.SECONDS.sleep(18);
      System.out.println("You: I swear on my life, I will make it big one day. Or else, I'd rather die.\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("    Your will burns strong. And now you've got your wish...\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("     ----------     ");
      TimeUnit.SECONDS.sleep(3);

      System.out.println("In the distance, you see a silhouette of a person on water's edge. He seems to be lying face-up \n"
          + "at the blue sky.\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("You: Hey there! My ship was damaged in the storm, can you tell me where we are?\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("The man doesn't seem to move, his body lays static.\n"); //Pun intended
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You: Hello?\n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("You approach him slowly, afraid of what he may be.\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You stop within an arm's reach...\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("His face is streaked by a suffocated blue; his mouth ajar as though he is reaching for \n"
          + "every breath of air.\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("He jumps up at you");
      TimeUnit.MILLISECONDS.sleep(700);
      System.out.println("and clings onto your leg");
      TimeUnit.MILLISECONDS.sleep(900);
      System.out.println("and pulls your collar down to his dark red eyes.\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("   Hmph\n");
      TimeUnit.MILLISECONDS.sleep(800);
      System.out.println("  Hmph\n");
      TimeUnit.MILLISECONDS.sleep(800);
      System.out.println("    Hmph\n");
      TimeUnit.MILLISECONDS.sleep(800);
      System.out.println("Fisher man: Please, help me. Help us. People are dying everywhere!\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("You: I'm sorry, I don't know what you are talking about? My ship was wrecked,\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Fisher man: Last month, after the man in the wood dies, a plague spreaded across our town. \n"
          + "            Everyone ignored it, thinking it was only the Winter Wind at first, but things turned out \n"
          + "            worse when they coughed up blood.\n");
      TimeUnit.SECONDS.sleep(7);
      System.out.println("            No one knew how to cure it, neither did the city hospital. They isolated us, \n"
          + "           thinking we will spread it to the city, and now we are left all alone.\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("Fisher man: People who weren't sick locked themselves in their houses while the ill-stricken \n"
          + "           turned savage. They pillaged homes and killed whoever was in it.\n");
      TimeUnit.SECONDS.sleep(5);
      System.out.println("Fisher man: Please help us, dear stranger. My son and wife are reaching their last breath.\n");
      TimeUnit.SECONDS.sleep(2);

      System.out.println("You try to pull yourself away, but his grip tightens, the final strength of a dying man.\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("And he drops dead on the sand.\n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("A: take the fisher man's fishing pole");
      System.out.println("B: lays the man's eyes to rest and continue walking");
      System.out.println("C: gather some firewood and set up for the night. This will give you some time to think");

      oldManMove();

      System.out.println("Only a few steps and you're reached the town's rotting welcome sign:");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("---------------------");
      System.out.println("-------WELCOME-------");
      System.out.println("---------TO----------");
      System.out.println("-----UNDERBRIDGE-----");
      System.out.println("---------------------\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You: Underbridge? I remember that name somewhere\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You do, because that's the name of the place where the treasure is buried. You've \n"
          + "been searching for it for your entire life. A bolt of excitement hits your feet, but it \n"
          + "retreats when you see a silhouette of a man approaching you.\n");
      TimeUnit.SECONDS.sleep(9);
      System.out.println("He has brown hair, that of a sailor. His body stands tall, almost taller than the \n"
          + "scorched trees planted on the pavements. You take your first step on concrete in what seems like \n"
          + "years. You approach the man more slowly, almost hesitantly this time, but still faking a friendly \n"
          + "smile. \n");
      TimeUnit.SECONDS.sleep(15);
      System.out.println("You: Hey there! I'm lost. Can you please tell me where we are? \n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Sailor: Hey! Finally... finally...\n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("His words fall on the pavement. Then he stumbles and collapses onto his bare hands. \n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Sailor: I'm the chief of this town. When the man in the Great Mansion died, the sea turned \n"
          + "       against us. When we set out, its waves turned against us, blowing in the opposite \n"
          + "       direction whichever way we went. When we returned to our sailor, as though we didn't bare \n"
          + "       enough bad news, our town turned into something different. Houses were broken down, \n"
          + "       our park was barren, and cries screeched from the far distance.\n");
      TimeUnit.SECONDS.sleep(25);
      System.out.println("He sat up, with your help against the pavement\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("Sailor: When we ran to our homes, our wives and children were gone. I found mine \n"
          + "       quivering in our silo, their eyes red like the sun's might and so was my newborn son... \n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("The sailor slumped forward, seemingly unable to hold himself still.\n");
      TimeUnit.SECONDS.sleep(1);
      System.out.println("Sailor: You've got to save us young man. I don't know who you are, but you've come here \n"
          + "       for a reason. If you don't believe me, please help us out of your kindness. Our women and children \n"
          + "       are dying in fear. Please, please, ple...\n");
      TimeUnit.SECONDS.sleep(7);
      System.out.println("Before he can say another word, he is coughing up blood.\n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("You: Okay, okay. How can I help?\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("Sailor: Please go to the witch's hut where the sun rises on the mountain's peak \n"
          + "       and our hospital, my wife is there right now. Please save her. I knew I was going to die, \n"
          + "       so I left the hospital to find help. And you are my last hope.\n");
      TimeUnit.SECONDS.sleep(8);
      System.out.println("The veins under his eyes are now swollen. He is right; he is going to die, but \n"
          + "his words echo in your mind. You will need help from the local to continue searching \n"
          + "for your treasure, but this foreign plague has been able to kill a whole town. Why \n"
          + "would you be any different? These thoughts bounce around in your mind until you decided: \n"
          + "this treasure will be yours. \n");
      TimeUnit.SECONDS.sleep(25);

      //Critical move chooses between exploring Witch Hut or Hospital
      //POSTCONDITION: Both paths explored before moving on
      criticalMove();

      //Story telling: going through the woods
      System.out.println("After days of wandering, you finally arrive at the edge of the forbidden woods.\n"
          + "There's a dark sense of mystery to the trees. Their bark look far darker than any trees \n"
          + "you have seen, almost charcoal, and their branches are thinly jagged. They don't look natural\n"
          + "by the standards of the trees everywhere else. But with your goal in mind, you take your first\n"
          + "step into the woods. No wind is pushing back against your movement, which is a good sign.\n"
          + "And comes the second step, and third, and forth. You are in the forbidden woods.\n");
      TimeUnit.SECONDS.sleep(28);
      System.out.println("But for days, even with the help of the witch's map, you don't find any sign of\n"
          + "a life, let alone a mansion. Your stomach is tearing at your torso and the chilly air of the\n"
          + "woods begin to pierce your skin. You are about to collapse to your knees. But just before your\n"
          + "hope dims completely, a house glimpses at the corner of your fatigued eyes.\n");
      TimeUnit.SECONDS.sleep(18);
      System.out.println("You see a cave filled with hanging vines\n"); //MD
      TimeUnit.SECONDS.sleep(4);
      System.out.println("A: enter the under-croft\n");
      String continueToUnderCroft = scanner.nextLine().toLowerCase();
      while (!continueToUnderCroft.equals("a")) { //Some interaction with user
        System.out.println("You have ventured so far, there is only one path now\n");
        continueToUnderCroft = scanner.nextLine().toLowerCase();
      }
      System.out.println("With a rekindled hope, you muster the last pieces of your strength and continue on.\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("You emerge from the dark, blood-stenched cave with shock.\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("A mansion lays in front of you, the gates slightly open and shakes slightly in the wind\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("   -------------------\n");
      System.out.println("   REPERE WOOD MANSION\n");
      System.out.println("   -------------------\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("You: So that's what the mansion is called? I guess no body has lived to tell the\n"
          + "     tales.\n");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("The mansion towers everything else in the distance. Its roof is slated perfectly,\n"
          + "even after isolation, it still glistens. Two tall graphite columns stands infront of the house,\n"
          + "as if protecting it. But after Tremens died, some moss has begun growing at the bottom of\n"
          + "those pillars.\n");
      TimeUnit.SECONDS.sleep(12);
      System.out.println("You open the creeking gate and enter.\n");
      TimeUnit.SECONDS.sleep(4);

      houseMove();

      //Explores mansion, painting,
      //Reads diary, house crumbles
      System.out.println("The bottom floor is completely empty; the only thing residing on its surface is\n"
          + "dust and insects. The house aged fast without its owner. And you make your way upstairs with\n"
          + "a spiral, wooden staircase. To your surprise, the second floor is still filled with furniture:\n"
          + "bookcase, books, a table lies in the distance, right in front of you, and a tall lamp stands\n"
          + "next to it. The second floor, strangely, aged much slower than the first. Perhaps the beastiality\n"
          + "of the woods work its way up from the bottom.\n");
      TimeUnit.SECONDS.sleep(26);


      System.out.println("On the table, you notice a book. It must be what you're looking for:\n");
      TimeUnit.SECONDS.sleep(3);

      System.out.println("The last page seems to be written in a different handwriting.\n");
      TimeUnit.SECONDS.sleep(3);

      System.out.println("-----THE MASTER'S DIARY-----\n");
      TimeUnit.SECONDS.sleep(3);

      player.examine(openDiary);
      TimeUnit.SECONDS.sleep(20);



      System.out.println("As you read the final page, the house starts to rumble loudly. Footsteps are heard\n"
          + "coming from the bottom of the house. An earthquake spreads through the house. Books fall from\n"
          + "their places all around you. You don't know what is happening, but you know you have to get out.\n");
      TimeUnit.SECONDS.sleep(12);

      escapeHouseMove();

      System.out.println("You wake up with a pain in your neck. A hunger runs rampage through your body.\n"
          + "Your bones hurt to their core. How long have been unconscious? You don't know, and you can't\n"
          + "know.\n");
      TimeUnit.SECONDS.sleep(8);

      System.out.println("But one thing is on your mind: the town. You must return to Albus, and return\n"
          + "him the diary, and save the town. With a brittled torso, you push yourself up and start your\n"
          + "trek back to town.\n");
      TimeUnit.SECONDS.sleep(9);

      System.out.println("After days, you finally arrive at the edge of the wood. You don't have much left\n"
          + "in yourself, but a few tears manage to swell up in your eyes. The look of the town remains\n"
          + "the same as when you left it: broken and desolate. But this time, a new hope lights your hope.\n"
          + "Perhaps, the town won't have to die. Perhaps, you can save the town.\n");
      TimeUnit.SECONDS.sleep(18);

      /* You return to the town,
      Reveals the secret of greed to the people at the hospital and starts sharing the secret through out the house
      */
      System.out.println("The hospital is still beaming with screams and people rushing, but you don't see\n"
          + "Albus anywhere. A hospital help you on your feet and transport you to an empty room. While on\n"
          + "the stretcher, you dream not about your treasure, but about Albus. The following day, a hospital\n"
          + "worker told you that Albus died two weeks ago. How long were you in the woods? It must have more\n"
          + "than several weeks.\n");
      TimeUnit.SECONDS.sleep(20);


      System.out.println("Tears roll down your face, at the lost of a good friend. Your will to find the\n"
          + "treasure dims at this heartbreak, but you decide to return to where your boat crashed,\n"
          + "and where you first arrived at this island nonetheless.\n");
      TimeUnit.SECONDS.sleep(12);

      System.out.println("When you arrive at the beach, still as blue and serene as when you first saw it,\n"
          + "a new boat lies in the wreckage's place. The boat is new, and couldn't have been there for long.\n");
      TimeUnit.SECONDS.sleep(8);

      System.out.println("You head in, and see a note rolled up and held in the steering wheel. It is a note\n"
          + "written by Albus:\n");
      TimeUnit.SECONDS.sleep(4);

      System.out.println("      To "+ player.name + ",");
      TimeUnit.SECONDS.sleep(4);
      System.out.println("      If you're reading this note, it means that you have saved the town. The diary\n"
          + "   that you took, it was not mine at all. It belongs to a kind-hearted ruler from the past. He\n"
          + "   treats his people with compassion and teaches his people to be kind to one another through out\n"
          + "   his life. When he died, he passed down his diary to his successor so that they, too, can practice\n"
          + "   his ways. The diary serves as a contract that you sign as soon as you touch it, to protect the\n"
          + "   town. So, yes, I lied to you. But whatever I told you when I came to you is true. I do share his blood...\n");
      TimeUnit.SECONDS.sleep(26);

      System.out.println("      So, now, " + player.name + ", you are the new ruler of this town by your deed.\n"
          + "   But this was not your decision. Before I died, I asked some of the hospital workers to fix your\n"
          + "   boat for you, so that you can still leave if you choose. But please consider my last wish before\n"
          + "   I die, dear " + player.name +". I would like you to take care of my town, but this time, the power\n"
          + "   is in your hands; I have no more lies, " + player.name + ". May you make the right choice.");
      TimeUnit.SECONDS.sleep(22);

      System.out.println("      Best wishes, ");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("      Albus Pristine\n");
      TimeUnit.SECONDS.sleep(6);

      System.out.println("THE BEACH\n");

      System.out.println("A: stay at the town and help its people");
      TimeUnit.SECONDS.sleep(2);

      System.out.println("B: continue on the journey you set off to complete");

      String finalMove = scanner.nextLine().toLowerCase();
      while (!(finalMove.equals("a") || finalMove.equals("b"))) {
        System.out.println("What do you choose?\n");
      }

      if (finalMove.equals("a")) {
        System.out.println("You decide to remain at the town and help its people overcome greed. Soon after,\n"
            + "the people of the town love you for your character and kindness. One day, you decide to return\n"
            + "to Repere Mansion, where you find a secret passage beneath the under-croft. In there, you find\n"
            + "the diary of all the other masters before you. It seems Albus' diary is missing in one the shelves.\n");
        TimeUnit.SECONDS.sleep(22);
        System.out.println("It is true: Albus never accepted the role of the master to be his life, and he fought with\n"
            + "all of his strength to oppose the Goddess. In the end, he remains the bravest soul in your eyes.\n"
            + "You take an unwritten notebook of the shelf, smooth your hand through its cover, and lay down\n"
            + "your first words as a master:\n");
        TimeUnit.SECONDS.sleep(22);

        System.out.println("    April 11th, 1989\n");

        System.out.println("    Today, I discovered the master's under-croft. I learnt the devastation of greed,\n"
            + "   and I am, now, the master of my people and my own self. I am now at peace with\n"
            + "   my past and present. The secret to life is without greed");
        TimeUnit.SECONDS.sleep(19);
        System.out.println("    All the best to whoever is reading this, ");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("    " + player.name + ", Master of The Undercroft");
        TimeUnit.SECONDS.sleep(5);

      } else if (finalMove.equals("b")) {
        System.out.println("You decide to continue on journey for wealth. But before you leave, you stay to\n"
            + "repair the town. The plague is receding, and the townspeople are recovering. One day,\n"
            + "you decide to bury the Master's diary in a place deep under the Repere Mansion. Without anyone\n"
            + "touching the diary, the cycle will never be reborn.  You will be the last master.\n");
        TimeUnit.SECONDS.sleep(16);

        System.out.println("Then, after the town has repaired, you continue on your search for treasure. For\n"
            + "days on endless waves, you finally found it. You return home, Germany, and sell your discovery\n"
            + "in a national auction. Your family never has to taste the dirt of poverty ever again.\n");
        TimeUnit.SECONDS.sleep(16);
      }

      //THE END

      System.out.println("-------------------THE END-----------------\n");
      TimeUnit.SECONDS.sleep(4);


      System.out.println("THANK YOU FOR BRAVING THROUGH THIS ADVENTURE, " + player.name.toUpperCase() + "\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("I HOPE YOU ENJOYED THIS STORY.\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("email me at benjaminle2002@gmail.com with any suggestions for part 2");
      TimeUnit.SECONDS.sleep(3);


      //Plug
      //System.out.println("Email me at benjaminle2002@gmail.com for part 2");
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  //Interact with player
  public static Character createCharacter(){
    try {
      Character player;

      Scanner userInput = new Scanner(System.in);
      String gender =""; //Gender to determine which gender class to instantiate
      boolean validGender = false;
      while (validGender == false) {
        System.out.println("Who do you identify yourself as? (m/f)");
        gender = userInput.nextLine();
        if (gender.equals("m") || userInput.equals("f")){
          validGender = true;
        }
      }
      System.out.println("What is your name?"); //Name
      String name = userInput.nextLine();

      int age = 0; //Age
      boolean validAge = false;
      while (validAge == false) {
        System.out.println("How old are you? (please enter an integer)");
        age = userInput.nextInt();
        if (age < 0) {
          System.out.println("Woah. You're travelling back in time there. How do I know you won't look"
              + " into the future and ruin the ending?");
        } else if ( age > 100) {
          System.out.println("You must have a brave heart. But we can't guarantee your safety playing this game."
              + " Remember, this adventure is not for the faint-hearted...");
          TimeUnit.SECONDS.sleep(3);
          System.out.println("Maybe you can try again in the afterlife?");
        } else {
          System.out.println("You can now start your adventure. " + name + ", "
              + "I'm sorry for your inquisitive heart... ");
          TimeUnit.SECONDS.sleep(6);
          validAge = true;
        }
      }
      if (gender.equals("m")){
        player = new Male(name, age);
        return player;
      } else if (gender.equals("f")) {
        player = new Female(name, age);
        return player;
      }
    } catch (Exception e) {
      System.out.println(e);
    }
    return null;
  }

  // for beachMove()
  static boolean hasTakenBottle = false;

  public static void beachMove() {
    try {
      String beachMove = scanner.nextLine().toLowerCase();
      while (!(beachMove.equals("a") || beachMove.equals("b"))) {
        System.out.println("I'm not sure I understand that move \n");
        beachMove = scanner.nextLine().toLowerCase();
      }
      if (beachMove.equals("a")) {
        System.out.println("Along your way, you feel nauseous trying to recall what happened to your boat. \n"
            + "The last object you saw was a pair of dark red eyes that shapes the leaves. They lingered, even as you \n"
            + "sink down below.\n");
        TimeUnit.SECONDS.sleep(4);
      } else if (beachMove.equals("b")) {
        System.out.println("You head back to scavenge for what is left inside of your boat. You push open the door to your \n"
            + "crew mate's, Anthony's, bed room. His lifeless body lies on the floor, his eyes still open. The windows \n"
            + "were locked. He must have figured out the incident too late and drowned on his way out.\n");
        TimeUnit.SECONDS.sleep(8);
        System.out.println("His waterbottle, still intact, lies next to his desk.\n");
        TimeUnit.SECONDS.sleep(3);
        if (hasTakenBottle == false ) //Only print if hasn't taken bottle
          System.out.println("A: take water bottle");
        System.out.println("B: leave room and head back to the beach");
        roomMove();
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public static void roomMove() {
    try {
      TimeUnit.SECONDS.sleep(3);
      String roomMove = scanner.nextLine().toLowerCase();
      while (!(roomMove.equals("a") || roomMove.equals("b"))) {
        System.out.println("I don't understand that move\n");
        roomMove = scanner.nextLine().toLowerCase();
      }

      if (roomMove.equals("a")) {
        player.take(waterBottle);
        hasTakenBottle = true;
        roomMove();
      } else if (roomMove.equals("b")) {
        System.out.println("BEACH SHORE");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("You arrive back in the shore\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("A: walk down the shore in search for life\n");
        TimeUnit.SECONDS.sleep(3);
        beachMove();
      }
    } catch (Exception e) {
      System.out.println(e);
    }

  }

  public static void oldManMove(){
    try {
      String oldManMove = scanner.nextLine().toLowerCase();
      while (!(oldManMove.equals("a") || oldManMove.equals("b") || oldManMove.equals("c"))) {
        System.out.println("I don't understand that move\n");
        oldManMove = scanner.nextLine().toLowerCase();
      }

      if (oldManMove.equals("a")) {
        player.take(fishingPole);
        oldManMove();
      } else if (oldManMove.equals("b")) {
        //Same
        System.out.println("With a heavy heart, you continue your journey, wondering what happened to the \n"
            + "fisherman's town. Whatever it is, you don't want to go near it. \n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println("Hours pass before you see a sign of life: the town. You don't want to, but as \n"
            + "though your legs cannot break the rhythm, they keep walking...\n");
        TimeUnit.SECONDS.sleep(5);

      } else if (oldManMove.equals("c")) {
        System.out.println("You sleep the night in an awfully-shaped sand bed.");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("------");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("------");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("------");
        TimeUnit.SECONDS.sleep(3);

        System.out.println("Before you wake up, the red eyes came back in your sleep. They stare deep into your \n"
            + "desire. You body convulses, trying to shake it away, but it grabs onto your heart \n"
            + "and wouldn't let go");
        TimeUnit.SECONDS.sleep(7);
        System.out.println("-----\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("MORNING");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("B continue wakling");
        String continueMove = scanner.nextLine().toLowerCase();
        while (!(continueMove.equals("b"))) {
          System.out.println("I don't understand that move");
          continueMove = scanner.nextLine().toLowerCase();
        }
        //Same
        System.out.println("With a heavy heart, you continue your journey, wondering what happened to the \n"
            + "fisher man's town. Whatever it is, you don't want to go near it. \n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println("Hours pass before you see a sign of life: a broken town. An invisible weight hangs \n"
            + "on its windows and houses. You don't want to, but as though your legs cannot break the rhythm, \n"
            + "you keep walking...\n");
        TimeUnit.SECONDS.sleep(10);
      }
    } catch (Exception e) {
      System.out.println(e);
    }

  }

  //Variables to keep track where player has been: witchhut, med center
  static boolean isWitchHut = false;
  static boolean isMedCenter = false;
  public static void criticalMove() {
    try {
      TimeUnit.SECONDS.sleep(3);
      System.out.println("TOWN HALL");
      if (!isWitchHut)
        System.out.println("A: heads to find witch shack on top of the Swaying Hills");
      TimeUnit.SECONDS.sleep(1);
      if (!isMedCenter)
        System.out.println("B: go to town's medical center");

      String criticalMove = scanner.nextLine().toLowerCase();
      while (!(criticalMove.equals("a") || criticalMove.equals("b"))) {
        System.out.println("I don't understand that move\n");
        criticalMove = scanner.nextLine().toLowerCase();
      }

      if (criticalMove.equals("a")) {
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "You arrive on top of a hill at a slanted hut made from rotten wood planks. The magnolia\n"
                + "tree in its garden shares the darkness of the house. Its branches so crooked and barren you feel \n"
                + "its tug on your heart. You climb up the doorstep and knock with the silver door knocker shaped like \n"
                + "a falling leaf. A few minutes pass as the afternoon wind thickens.\n");
        TimeUnit.SECONDS.sleep(14);
        System.out.println(
            "But no one comes to answer you. You trying turning the knob and to your surprise, or not, "
                + "the knob twists under your hand.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("A: enter the mysterious house, seeing that no one is home");
        String enter = scanner.nextLine().toLowerCase();
        while (!enter.equals("a")) {
          System.out.println("The wind is nudging you into the house");
          enter = scanner.nextLine().toLowerCase();
        }
        TimeUnit.SECONDS.sleep(1);
        System.out.println(
            "You crack the door open slowly; a night-like darkness fills the house. You cannot\n"
                + "see your way, but you finally carry your whole body through the door.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "But the door slams shut with your back against it as though it acted deliberately.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("The wind hisses through the door crack.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out
            .println("A window crams shut in the distance, somewhere down the lightless hall.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println("Witch: Well, well, well. Hello there!\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "You cannot pick out her figure but her voice sounds as though it comes from 4 feet away.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("Witch: It seems like we have a visitor here.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "Suddenly, you feel the push of something against your throat. It grabs hold of your\n"
                + "wind pipe and doesn't allow you to breathe. The witch screams at the top of her lungs:\n");
        TimeUnit.SECONDS.sleep(8);
        System.out.println(
            "Witch: Why did you come here? Who brought you here? Is it Albus? If he did, tell him\n"
                + "       I don't want anything to do with him. The town can die. That doesn't need my help.\n");
        TimeUnit.SECONDS.sleep(7);
        System.out.println(
            "You: Oh no. It wasn't Albus. It wasn't. I stumbled upon here accidentally."); //You explain yourself
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
                  "    My ship was damaged from a storm, the last thing I remembered was clinging hard\n"
                + "    onto a piece of the ship. The next thing I knew, I woke up on this strange island with no way out.\n"
                + "    I found a man while walking down the shore but he died in my arms before he was able to tell me\n"
                + "    anything. I wandered for days and was about to give up when I saw your house up on this hill.\n"
                + "    So please help me. I have nothing left and nowhere to go.\n");
        TimeUnit.SECONDS.sleep(20);
        System.out.println(
            "Witch: Okay, before I tell you anything more, you must do something for me.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println(
            "She waves a hand-motion in the air, and suddenly, you feel much fuller with life.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Witch: Complete these three tasks for me and I will tell you what you want to know.\n");
        TimeUnit.SECONDS.sleep(3);

        //Explanation of three tasks
        System.out.println(
            "    First, an old friend of mine still owes me something I lent him a few lives ago.\n"
                + "   He resides in the underworld and has a favorite pet dog called Cerberus. Why don't you stop\n"
                + "   by his house and give me back what's mine?\n");
        TimeUnit.SECONDS.sleep(10);
        System.out.println(
            "    Second, an unfortunate object has landed on our island recently. I sense its power\n"
                + "   in the village but can't identify what it is. I know it resides down Alley Street. Why don't you\n"
                + "   go get me its last tear?\n");
        TimeUnit.SECONDS.sleep(10);
        System.out.println(
            "    Third, the master of potion is who I desire. But with my ill will, he won't conspire.\n"
                + "   He lives in the forest up North. Why don't you go ask him if he can make his luckiest potion for you\n"
                + "   and me?\n");
        TimeUnit.SECONDS.sleep(9);
        System.out.println(
            "With an irresistible force, you are pushed out of the door by the witch. The door slams\n"
                + "shut before you, unwilling to sway. You know you have to do this to get to your treasure.\n");

        witchHutMove();

        System.out.println(
            "You return to the mountain top, and the witch is surprised that you survived the tasks."); //MD
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "The witch welcomes you in, personally this time, with a different excitement to her\n"
                + "feet. You are the first mortal to have completed this task for her and she is now willing to \n"
                + "tell you what you want.\n");
        TimeUnit.SECONDS.sleep(9);
        System.out.println(
            "Witch: Okay... Before I came to life, there was a man who lived in the under-croft \n"
                + "     beneath the forbidden woods. His reign is so large that every animal in the wood is supposed \n"
                + "     to be under his commands. Even with all that power, the old man lives, for all his life, by \n"
                + "     himself. Last month, when he supposedly dies, the mansion was left to itself. Some men from \n"
                + "     the town tried to find their ways to it, but died trying. The men's bodies were nowhere to be \n"
                + "     found.\n");
        TimeUnit.SECONDS.sleep(22);
        System.out.println("The witch takes a deep breath.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "Witch: But only a week later, people in the town were infected with some sort of coughing disease.\n"
                + "     Soon after, their bodies were either paralyzed or became vile. You have probably heard of this,\n"
                + "     but people turned savage amongst one another, killing women and children thinking that somehow\n"
                + "     that will help. Those savages died even quicker than others. After all, although this is only my\n"
                + "     theory, I believe something was awoken by the man's death, somewhere deep inside his mansion...\n");
        TimeUnit.SECONDS.sleep(27);
        System.out.println(
            "Witch: Since you've helped me, I can ease your way to the mansion to with map of the \n"
                + "     forbidden woods. Follow the marks carefully and you should find your way there. Now, now, \n"
                + "     begone strange, I have my own things to take care of.\n");
        TimeUnit.SECONDS.sleep(14);
        System.out.println(
            "And again, you are forced out of the house by an invisible force, but this time, \n"
                + "the house vanishes after you're out of the entrance. Only a door remains.\n");
        TimeUnit.SECONDS.sleep(10);
        System.out.println("A sound echos from thin air: "
            + "   \"\uD835\uDD7F\uD835\uDD8D\uD835\uDD8A \uD835\uDD95\uD835\uDD86\uD835\uDD8E\uD835\uDD93 \uD835\uDD8E\uD835\uDD93 \uD835\uDD99\uD835\uDD8D\uD835\uDD8A \uD835\uDD98\uD835\uDD8E\uD835\uDD93\uD835\uDD93\uD835\uDD8A\uD835\uDD97 \uD835\uDD91\uD835\uDD8E\uD835\uDD8A\uD835\uDD98 \uD835\uDD8E\uD835\uDD93 \uD835\uDD99\uD835\uDD8D\uD835\uDD8A \uD835\uDD92\uD835\uDD86\uD835\uDD98\uD835\uDD99\uD835\uDD8A\uD835\uDD97'\uD835\uDD98 \uD835\uDD88\uD835\uDD97\uD835\uDD94\uD835\uDD8B\uD835\uDD99\"  \n");
        TimeUnit.SECONDS.sleep(9);
        player.take(mapThrForbiddenWood);
        TimeUnit.SECONDS.sleep(1);
        player.examine(mapThrForbiddenWood);
        isWitchHut = true;
        if (isMedCenter != true) {
          criticalMove();
        }
      } else if (criticalMove.equals("b")) {
        System.out.println(
            "You proceed to visit the town's hospital. By the sound of it, you don't think \n"
                + "it can do a lot to save the town, but perhaps you can learn something useful for your journey \n"
                + "to find treasure.\n");
        TimeUnit.SECONDS.sleep(8);
        System.out.println(
            "After only 40 minutes of walking, you arrive at the center. Unsurprisingly, it \n"
                + "looks worn down from the gloomy and desolate air of the town. Only few windows are lit up \n"
                + "with light bulbs. They must be holding operations in there while the other rooms are reserved \n"
                + "for helpless patients. \n");
        TimeUnit.SECONDS.sleep(10);
        System.out.println(
            "You make your way up the marble stairs to the hospital. Before you see anybody, \n"
                + "the stinking air of sick people already pierce your nose. The next thing you sense is the \n"
                + "belching of a senior doctor, looking to be around the age of forty.\n");
        TimeUnit.SECONDS.sleep(12);
        System.out.println("Doctor Albus: Hey you! Come here and give us a hand\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("The foreign doctor was pointing at you...\n");
        TimeUnit.SECONDS.sleep(4);

        medCenterMove();

        isMedCenter = true;
        if (isWitchHut != true) {
          criticalMove();
        }
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  //Witch Hut Move
  //Variables to keep track of the items collected
  static boolean isHadesVisit = false;
  static boolean isLastHorcrux = false;
  static boolean isHalfBloodPrince = false;
  static boolean isHellFirstTime = true;
  public static void witchHutMove() {
    try {
      //Speed run through tasks
      if (!isHadesVisit) System.out.println("A: find Hades's Ring");
      if (!isLastHorcrux) System.out.println("B: search for the last horcrux");
      if (!isHalfBloodPrince) System.out.println("C: search for the half-blood prince");

      String witchHutMove = scanner.nextLine().toLowerCase();
      while (!(witchHutMove.equals("a") || witchHutMove.equals("b") || witchHutMove.equals("c"))) {
        System.out.println("I don't understand that move");
        witchHutMove = scanner.nextLine().toLowerCase();
      }

      if (witchHutMove.equals("b")) { //Visits last horcrux
        TimeUnit.SECONDS.sleep(3);
        System.out.println("THE LAST HORCRUX");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("As the witch said, you reach the only house in Alley Street and a man stand waiting \n"
            + "for you. He knows why you are here, but unexpectedly, his eyes share the same velvet color \n"
            + "the old man and the sailor had.\n");
        TimeUnit.SECONDS.sleep(6);
        System.out.println("The Last Horcrux: I know why you are here. You are another one of Lilly's henchmen \n"
            + "       aren't you? Seeing that I'm dying anyway, I'll give her what she wants. But you have to \n"
            + "       answer my riddle first:\n");
        TimeUnit.SECONDS.sleep(7);
        System.out.println("------- POTTER'S RIDDLE --------");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("I am an empty void. Throw all the gold in the world and not a single inch of me will be filled.");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("I resound in everything, everyone... release me, if you can, and you will reach peace.");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("What am I?"); // The intended answer is greed
        String theLastHorcruxAnswer = scanner.nextLine().toLowerCase();
        System.out.println(" "); // Intended space
        while (!theLastHorcruxAnswer.equals("greed")) {
          youDied();
          theLastHorcruxAnswer = scanner.nextLine().toLowerCase();
        }

        System.out.println("Suddenly, the last horcrux drops to the ground. His eyes shine bright even in death, \n"
            + "as though he has finally found a peace within himself.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("The Last Horcrux: Take my tears. May you use it well as my last gift to this town.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("A: use a potion bottle nearby to collect James' tears.");
        String collectTears = scanner.nextLine().toLowerCase();
        while (!collectTears.equals("a")) {
          System.out.println("Collect his tears! His time is running out.");
          collectTears = scanner.nextLine().toLowerCase();
        }
        player.take(jamesTears);
        System.out.println("Some dark, crumbled bills appear in your pocket"); //Bill

        System.out.println("You wonder what happened to James before he died in your arms. His eyes shared the same\n"
            + "redness of all the others who died in your arms. But you have to return to the quest you are on.\n");
        TimeUnit.SECONDS.sleep(5);
        isLastHorcrux = true; //Player has arrived at The Last Horcrux's house
        TimeUnit.SECONDS.sleep(3);
        System.out.println("DOWNTOWN\n");
        TimeUnit.SECONDS.sleep(1);
        witchHutMove();
      } else if (witchHutMove.equals("c")) {
        System.out.println(
            "You continue to the woods and find a man meditating on a plank in the middle of the lake. \n"
                + "The man's eyes are shut tight. The only movements around are the shuffling of leaves and soil.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("A: jumps in the lake");
        System.out.println("B: calls out to him");
        System.out.println("C: drink the murky water\n");
        TimeUnit.SECONDS.sleep(3);
        String halfBloodPrinceMove = scanner.nextLine().toLowerCase();
        while (!(halfBloodPrinceMove.equals("a") || halfBloodPrinceMove.equals("b") || halfBloodPrinceMove.equals("c"))) {
          System.out.println("I don't understand that move");
          halfBloodPrinceMove = scanner.nextLine().toLowerCase();
        }

        while (halfBloodPrinceMove.equals("a") || halfBloodPrinceMove.equals("b")) {
          if (halfBloodPrinceMove.equals("a")) {
            System.out.println("The torrents from the lake pulls you down.");
            TimeUnit.SECONDS.sleep(2);
            System.out.println(
                "Like an invisible force, it slowly climbs up your body and eats you alive.\n");
            TimeUnit.SECONDS.sleep(2);
            youDied();
          }
          if (halfBloodPrinceMove.equals("b")) {
            System.out.println("You shout out to him at the top of your lungs, but nothing happens");
            TimeUnit.SECONDS.sleep(2);
            System.out.println("The man doesn't move in your presence.");
            TimeUnit.SECONDS.sleep(3);
            System.out.println("What's next?");
          }
          halfBloodPrinceMove = scanner.nextLine().toLowerCase();
        }

        System.out.println("You are transformed to a colorful house with the same man, but on his feet and \n"
            + "smiling contently.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("Sev: Hello, hello!\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("You: Hi. I was sent here by the witch atop the hill. I'm told you should be able \n"
            + "   to brew for her the luckiest potion?\n");
        TimeUnit.SECONDS.sleep(6);
        System.out.println("Sev: Yes, it is true I can brew the luckiest potion in the world. But why should \n"
            + "   I do that for her? or even you?\n");
        TimeUnit.SECONDS.sleep(5);

        //You tell him your story
        System.out.println("You explain to the potion master the story of how you arrived on this island without \n"
            + "any help and about your dream of leaving East Germany for good. Sev is shaken by your bravery \n"
            + "and perhaps a little bit by your naivety. But he decides to help you on your adventure nonetheless.\n");
        TimeUnit.SECONDS.sleep(14);

        System.out.println("Sev: Okay, I praise your determination and kind heart. I'll brew you a lucky potion,\n"
            + "but remember to use it wisely. You only have one chance.\n");
        TimeUnit.SECONDS.sleep(7);

        isHalfBloodPrince = true;
        System.out.println("You thank Sev for deciding to help you out. The process takes two long days and two\n"
            + "tireless nights. In the end, the potion comes out to be of a bright pink elixir.\n");
        TimeUnit.SECONDS.sleep(7);

        System.out.println("Sev hands it to you, and you continue on your way.\n");
        TimeUnit.SECONDS.sleep(5);

        player.take(luckyPotion);
        System.out.println("Giving a final look at the decorated house, you now only see the same man meditating in \n"
            + "the middle of the lake. The witches and wizards of this town must have knack for tricking \n"
            + "the mind, you thought.\n");
        TimeUnit.SECONDS.sleep(12);
        System.out.println("You finally return to the town.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("DOWNTOWN \n");
        TimeUnit.SECONDS.sleep(1);

        witchHutMove();
      } else if (witchHutMove.equals("a")) {
        if (!isLastHorcrux) { //If player hasn't visited last horcrux
          if (isHellFirstTime) {
            System.out.println("After days of wandering hopelessly, you manage to find your road to hell. You \n"
                + "have wandered through out the town for another hint, but nothing comes. You came back \n"
                + "to the witch's hut, but the house is still gone. However, when you returned, something \n"
                + "popped into your mind. A man had been at staying around the mountain's foot for five \n"
                + "consecutive days. When you ask him why he is staying at the mountain's foot, he replied \n"
                + "that he wants to end his life.\n");
            TimeUnit.SECONDS.sleep(22);

            System.out.println("Then, you realized the moutain must be the entrance to hell. After a few more days \n"
                + "circumventing it, you finally comes at the entrance marked by weird lettering.\n");
            TimeUnit.SECONDS.sleep(4);
            System.out.println("             GATES TO UNDERWORLD  \n");
            TimeUnit.SECONDS.sleep(2);
            System.out.println("    \uD835\uDD17\uD835\uDD25\uD835\uDD22 \uD835\uDD05\uD835\uDD2F\uD835\uDD1E\uD835\uDD33\uD835\uDD22 ℌ\uD835\uDD22\uD835\uDD1E\uD835\uDD2F\uD835\uDD31 ℭ\uD835\uDD2C\uD835\uDD2A\uD835\uDD22\uD835\uDD30 \uD835\uDD18\uD835\uDD2B\uD835\uDD30\uD835\uDD25\uD835\uDD1E\uD835\uDD31\uD835\uDD31\uD835\uDD22\uD835\uDD2F\uD835\uDD22\uD835\uDD21\n");
            TimeUnit.SECONDS.sleep(4);
            System.out.println("You enter and is greeted by a man of hollow face and a baritone voice.\n");
            TimeUnit.SECONDS.sleep(3);
          } else {
            TimeUnit.SECONDS.sleep(4);
            System.out.println("    GATES TO UNDERWORLD\n");
            TimeUnit.SECONDS.sleep(2);
          }
          System.out.println("Charon: Give me three death shillings, and I'll let you pass.\n");
          TimeUnit.SECONDS.sleep(3);
          System.out.println("You: What are death shillings?\n");
          TimeUnit.SECONDS.sleep(2);
          System.out.println("Charon: It's not my responsibility to answer your silly questions.\n");
          TimeUnit.SECONDS.sleep(2);
          System.out.println("You: But...\n");
          TimeUnit.SECONDS.sleep(2);
          System.out.println("Charon: Be gone, mortal. And come back with something you can pay with.\n"
              + "       Either your heart or your wit.\n");
          TimeUnit.SECONDS.sleep(4);
          System.out.println("You are forced to return back to town to find another way into hell.\n");
          TimeUnit.SECONDS.sleep(3);

          witchHutMove();
        } else if (!isHalfBloodPrince) { //If player hasn't visited half-blood prince
          if (isHellFirstTime) {
            System.out.println("After days of wandering hopelessly, you manage to find your road to hell. You \n"
                + "have wandered through out the town for another hint, but nothing comes. You came back \n"
                + "to the witch's hut, but the house is still gone. However, when you returned, something \n"
                + "popped into your mind. A man had been at staying around the mountain's foot for five \n"
                + "consecutive days. When you asked him why he is staying at the mountain's foot, he replied \n"
                + "that he wants to end his life.\n");
            TimeUnit.SECONDS.sleep(20);

            System.out.println("Then, you realized the moutain must be the entrance to hell. After a few more days \n"
                + "circumventing it, you finally comes at the entrance marked by weird lettering.\n");
            TimeUnit.SECONDS.sleep(4);
            System.out.println("          GATES TO UNDERWORLD    ");
            TimeUnit.SECONDS.sleep(2);
            System.out.println("    \uD835\uDD17\uD835\uDD25\uD835\uDD22 \uD835\uDD05\uD835\uDD2F\uD835\uDD1E\uD835\uDD33\uD835\uDD22 ℌ\uD835\uDD22\uD835\uDD1E\uD835\uDD2F\uD835\uDD31 ℭ\uD835\uDD2C\uD835\uDD2A\uD835\uDD22\uD835\uDD30 \uD835\uDD18\uD835\uDD2B\uD835\uDD30\uD835\uDD25\uD835\uDD1E\uD835\uDD31\uD835\uDD31\uD835\uDD22\uD835\uDD2F\uD835\uDD22\uD835\uDD21\n");
            TimeUnit.SECONDS.sleep(4);
            System.out.println("You enter and is greeted by a man of hollow face and a baritone voice.\n");
            TimeUnit.SECONDS.sleep(3);
          } else {
            TimeUnit.SECONDS.sleep(4);
            System.out.println("GATES TO UNDERWORLD");
            TimeUnit.SECONDS.sleep(3);
          }
          System.out.println("You hand the ferrymen the crumbled bills that you received from The Last Horcrux, \n"
              + "and he agrees to carry you through the the deadly lake on his ferry. The whole journey is \n"
              + "dark with only Charon's orange lamp leading the way. After some time, you arrive at the \n"
              + "underworld, marked by red lanterns floating high above.\n");
          TimeUnit.SECONDS.sleep(15);
          System.out.println("Finally, you arrive at a charcoal mansion and a door filled with scratch marks. \n"
              + "A man comes out to greet you. He wears a crown made from burnt tree bark and a robe that sets \n"
              + "sparks of fire going where ever it thouches. He must be Hades, you thought.\n");
          TimeUnit.SECONDS.sleep(10);
          System.out.println("Again, you explain your situation to the uninterested man. How your boat crashed \n"
              + "and you are left all alone. But Hades is rarely interested in the mortal's business. As \n"
              + "he sees it, the more people died, the more wood he has to burn and his Cerberus to eat.");
          System.out.println("Without any luck on your side, Hades is unwilling to give in\n");
          TimeUnit.SECONDS.sleep(15);
          System.out.println("You are forced to return to the overworld to find another option out.\n");
          TimeUnit.SECONDS.sleep(5);
          System.out.println("DOWNTOWN\n");
          TimeUnit.SECONDS.sleep(3);

          witchHutMove();
        } else {
          System.out.println("Again, you enter the world and hands Charon the death bills and is carried to\n"
              + "Hades' mansion. This time, with the lucky potion, Hades feel a sudden compulsion to help you\n"
              + "with your journey. He pulls the ring off of his ring finger and hands to the shining \n"
              + "emerald. He even pays the return fee to Charon for you. You realize then the power of\n"
              + "luck in life. \n");
          TimeUnit.SECONDS.sleep(18);

          player.take(hadesRing);
          TimeUnit.SECONDS.sleep(2);
          System.out.println("With all your destinations achieved, you head back to the overworld with a\n"
              + "strange, unfitting, overwhelming joy.\n");
          TimeUnit.SECONDS.sleep(5);

        }
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  //Part of Med Center Move

  static String[] playerQuestion = new String[5];
  static String[] doctorResponse = new String[5]; //Random response from the doctor

  static boolean hasExitHospital = false;
  public static void medCenterMove() {
    try {
      System.out.println("TOWN HOSPITAL");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("A: ask the doctor about what happened to the town");
      if (hasExitHospital == false) {
        System.out.println("B: exit hospital, too sick of taking orders from strangers");
      }
      System.out.println("C: help out at the hospital\n");

      String medCenterMove = scanner.nextLine().toLowerCase();
      while (!(medCenterMove.equals("a") || medCenterMove.equals("b") || medCenterMove
          .equals("c"))) {
        System.out.println("You have 3 options in your mind: a, b, or c");
        medCenterMove = scanner.nextLine().toLowerCase();
      }

      if (medCenterMove.equals("a")) {
        System.out
            .println(player.getName() + ": " + playerQuestion[(int) (Math.random() * 5)] + "\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("Doctor Albus: " + doctorResponse[(int) (Math.random() * 5)] + "\n");
        TimeUnit.SECONDS.sleep(2);
        medCenterMove();
      } else if (medCenterMove.equals("b")) {
        TimeUnit.SECONDS.sleep(3);
        System.out.println("You wander for days without food, water, nor company.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println(
            "Finally, when a pang pierces through your torso, you head back to the hostpital for help.\n");
        TimeUnit.SECONDS.sleep(4);
        hasExitHospital = true;
        medCenterMove();
      } else if (medCenterMove.equals("c")) {
        System.out.println("You finally decide to help the dying hospital.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Doctor Albus: Hey. Thank you man. I need you to carry these buckets down to the \n"
                + "     basement for me. And report to me when you're done. I'll be running around.\n");
        TimeUnit.SECONDS.sleep(8);
        System.out.println(
            "You look into the yellow buckets, and they are filled with all kinds of body excretion: \n"
                + "vomit and feces. You close your eyes, hold your breaths, and put on some plastic gloves you see on \n"
                + "the desk infront of you. And you muscle yourself to complete the deed.\n");
        TimeUnit.SECONDS.sleep(12);
        System.out.println(
            "Working at the hospital comes with a benefit: you have a bed with pillows to sleep on, \n"
                + "and you have food to eat. After a week, you begin to get used to the routine around the place. You wake up, \n"
                + "brush your teeth without toothbrush, and report to Albus to let him know that you're ready for work.\n");
        TimeUnit.SECONDS.sleep(12);
        System.out.println(
            "Meals are served twice a day, during mid-day and mid-afternoon, but you can't complain. \n"
                + "At least you have something in your stomache to sleep through the night.\n");
        TimeUnit.SECONDS.sleep(8);
        player.thirst -= 0.5;
        System.out.println(
            "One day, Albus approaches you with a pair of swollen eyes.\n");
        TimeUnit.SECONDS.sleep(5);
        /* MD: Doctor reveals he is infected, tells you the story of the man on the hill, and tells you how to get there */
        System.out.println(
            "Albus: Hi " + player.name + ". I hope I'm not interrupting your routine here. \n"
                + "     *coughs repeatedly* As you can probably tell, I've been infected with the disease. I don't \n"
                + "     think I have a lot of time left, so I need you to do something for me, okay, "
                + player.name + ".\n");
        TimeUnit.SECONDS.sleep(11);
        System.out.println(
            "Albus: I'm going to tell you something that I've never told any stranger about our \n"
                + "     town. And with that knowledge, I need you to save it.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "You: But hold on Albus. Why are you saying this to me? And no one else? I'm only a stranger\n"
                + "     to you.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println("Albus: And that is exactly why " + player.name
            + ". Because you are new to the island\n"
            + "     you have the most time left in you. But more than that, you don't share the same characteristic\n"
            + "     most of us on this island have.\n");
        TimeUnit.SECONDS.sleep(12);
        System.out.println("You: And what is that?\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("Albus: Greed, " + player.name + ". Greed.\n");
        TimeUnit.SECONDS.sleep(6);
        System.out.println("You: Okay. So, what were you going to tell me?\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("Albus: Do you know the man in the wood?\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "You: No... But I've heard his stories multiple times. He died last month right?\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Albus: Yes, and I was born to take his role in the woodland mansion, but I knew \n"
                + "     how lonely my life would become, so I escaped. I was followed by the Goddess whose name I \n"
                + "     I can't reveal for days until I stumbled upon Tremens, that is his name, the man in the \n"
                + "     woods. I told him my story, and he agreed to help as long as I take care of his town. I \n"
                + "     guess we both thought we escaped a prison-like life, but we only ended up in a different \n"
                + "     cell. The Goddess stopped chasing me now that she had a new replacement.\n");
        TimeUnit.SECONDS.sleep(28);
        System.out.println("You: Okay, so what do you want me to do?\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println("Albus: I need you to go to the woodland mansion, " + player.name
            + ". And find out the secret\n"
            + "     of its existence. You see, I escaped at a very young age, and I tried to disconnect from that\n"
            + "     past decades ago. But I know, in one way or another, when Tremens died last month, the Godddess\n"
            + "     must be seeking for a new replacement again.\n");
        TimeUnit.SECONDS.sleep(14);
        System.out.println("You: So why can't you return to her?\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Albus: I tried. Believe me, I tried. But when I returned two weeks ago, a strong\n"
                + "     current blew against my movement as I try to enter the woods in every direction. I don't\n"
                + "     think I can be accepted again to fit the role.\n");
        TimeUnit.SECONDS.sleep(6);
        System.out.println("You: So you want me to take up the role?\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println(
            "Albus: No, not quite, " + player.name + ". I need you to go to the woodland mansion\n"
                + "     and retrieve me my diary. I wrote down all my knowledge of the Keeper's role into that book.\n"
                + "     The diary will tell you what to do next.\n");
        TimeUnit.SECONDS.sleep(10);
        System.out.println("You: Okay. I'll consider your request.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Albus is on his knees now, perhaps too tired to stand himself up. He is coughing\n"
                + "up blood again. But before he makes his way out of your room, he leaves a final remark.\n");
        TimeUnit.SECONDS.sleep(7);
        System.out.println(
            "Albus: Please, " + player.name + ". Seriously consider it. The fate of the whole \n"
                + "     town and its people depend on you.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "The both of you lock eyes before you try to look away and Albus closes the door behind him.\n");
        TimeUnit.SECONDS.sleep(8);
        System.out.println("After two days, you come to Albus' bedroom.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("You: Okay, Albus. I'll accept your request but under one condition.\n");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("Albus: Sure, " + player.name + ". What may that be?\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "You: You must tell me the location of the treasure on this island whenever I'll\n"
                + "     be done.\n");
        TimeUnit.SECONDS.sleep(4);
        System.out.println(
            "Albus: Oh do you mean the Tretter treasure? *grins lightly* Sure, " + player.name
                + ".\n"
                + "     Whatever you need to know.\n");
        TimeUnit.SECONDS.sleep(5);
        System.out.println(
            "In the following day, you pack your bags with a week-full of food, and embark on your\n"
                + "unfinished journey without a goodbye to Albus. Something inside you trembles at what \n"
                + "lay ahead.\n"); //MD
        TimeUnit.SECONDS.sleep(4);
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  //Fill Player and Doctor question bank for med center scene
  public static void fillPlayerQuestion() {
    playerQuestion[0] = "Can you help me?";
    playerQuestion[1] = "Is this the only functioning hospital in this town?";
    playerQuestion[2] = "Hey sir, can you help me?";
    playerQuestion[3] = "The people, why are their eyes dark red and their body so gaunt?";
    playerQuestion[4] = "Why do you and some people not get infected?";
  }

  public static void fillDoctorResponse() {
    doctorResponse[0] = "Can't you see I'm busy?";
    doctorResponse[1] = "What do you want?";
    doctorResponse[2] = "I don't have time for you. Can't you see I'm busy?";
    doctorResponse[3] = "Under no circumstances will I answer you";
    doctorResponse[4] = "I'll tell you the truth if you choose to help me";
  }

  //Final Part: House

  //For house move
  static boolean hasEatSandwich = false;
  static boolean hasExploredGarden = false;

  public static void houseMove() {
    try {
      if (hasEatSandwich == false)
        System.out
            .println("A: eat your final sandwich continuing on, considering that it \n"
                + "   probably will be the last of your life");

      if (hasExploredGarden == false)
        System.out.println(
            "B: explore the garden of the mansion to search for anything that might be helpful");

      System.out.println("C: enter the house, nervous of what you may find inside...\n");
      String houseMove = scanner.nextLine().toLowerCase();
      while (!(houseMove.equals("a") || houseMove.equals("b") || houseMove.equals("c"))) {
        System.out.println("The time is now. Make one decision or the other.\n");
        TimeUnit.SECONDS.sleep(2);
        houseMove = scanner.nextLine().toLowerCase();
      }

      if (houseMove.equals("a")) {
        System.out.println("Although a bit dampened, the sandwich tastes amazing in your mouth.\n");
        TimeUnit.SECONDS.sleep(2);
        player.hunger -= 0.5;
        player.thirst -= 0.5;
        player.fatigue -= 0.5;
        player.checkTiredness();
        hasEatSandwich = true;
        houseMove();
      } else if (houseMove.equals("b")) {
        //MD: garden
        System.out.println("You wander around the dead garden, and you find a pair of broken glasses\n");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("A: take pair broken glasses\n");
        String takeGlass = scanner.nextLine().toLowerCase();
        while (!takeGlass.equals("a")) {
          System.out.println("You: Hmm, this glass looks curious\n");
          takeGlass = scanner.nextLine().toLowerCase();
        }
        player.take(brokenGlasses);
        TimeUnit.SECONDS.sleep(2);
        System.out.println(brokenGlasses.getDescription());
        TimeUnit.SECONDS.sleep(5);
        System.out.println(""); // Intentional space
        hasExploredGarden = true;
        houseMove();
      } else if (houseMove.equals("c")) {
        System.out
            .println("You make your way up the marble stairs and open the front door. It's tall,\n"
                + "almost twice your height, and heavy. As it opens, it lets out a loud roar, and a cold rush\n"
                + "of air floods your face. No one seems to be home. The floor makes cracking noise as you put\n"
                + "your weight on the rotting planks.\n");
        TimeUnit.SECONDS.sleep(10);
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }


  static boolean hasTakeDiary = false;
  public static void escapeHouseMove() {
    try {
      if (hasTakeDiary == false)
        System.out.println("A: takes the diary");
      System.out.println("B: dashes out of the windows despite the shattered window\n");
      String escapeMove = scanner.nextLine().toLowerCase();
      while (!(escapeMove.equals("a") || escapeMove.equals("b"))) {
        System.out.println("Come on! Time's ticking!");
        escapeMove = scanner.nextLine().toLowerCase();
      }

      if (escapeMove.equals("a")) {
        player.take(openDiary);
        hasTakeDiary = true;
        System.out.println("Come on! Time's ticking!");
        TimeUnit.SECONDS.sleep(1);
        escapeHouseMove();
      } else if (escapeMove.equals("b")) {
        System.out.println("With a great thump, you crash on the soft grass and pass out.");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("----------------");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("----------------");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("----------------");
        TimeUnit.SECONDS.sleep(2);
        System.out.println("----------------\n");
        TimeUnit.SECONDS.sleep(5);
      }
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public static void youDied() {
    try {
      System.out.println("-------------YOU DIED-------------");
      System.out.println("It is clear you haven't learn the lesson of a bottomless greed");
      System.out.println("May uneasiness follow you everywhere you go...");
      System.out.println("----------------------------------\n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("Try that again?");
    } catch (Exception e) {
      System.out.println(e);
    }
  }
}



