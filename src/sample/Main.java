package sample;

import java.util.concurrent.TimeUnit;

public class Main {

  public static void main(String[] args) {
    try {
      System.out.println();
      System.out.println("  WELCOME TO THE TEXT-BASED ADVENTURE GAME \n");
      TimeUnit.SECONDS.sleep(2);
      System.out.println("          THE MASTER'S UNDERCROFT       \n");
      TimeUnit.SECONDS.sleep(3);
      System.out.println("This game will be choice-based. Please enter only the beginning letter\n"
          + "of each option - a, b, or c - when prompted. Let's get started!\n");
      TimeUnit.SECONDS.sleep(4);
      Story story = new Story();

    } catch (Exception e) {
      System.out.println(e);
    }
  }
}
