package sample;

import java.util.Scanner;

public abstract class Character {
  protected String name;
  protected int age;
  protected int fatigue;
  protected int hunger;
  protected int thirst;
  protected Item[] inventory;


  //GETTERS and SETTERS
  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }

  public int getFatigue() {
    return fatigue;
  }

  public int getHunger() {
    return hunger;
  }

  public int getThirst() {
    return thirst;
  }

  public void setThirst(int thirst) {
    this.thirst = thirst;
  }

  //Methods
  public void printFatigue(){
    String result = "FATIGUE:  (";
    for (int i =0; i < fatigue; i++) {
      result += "--";
    }
    for (int i = 0; i < 10-fatigue; i++) {
      result += "  ";
    }
    result += ")";
    System.out.println(result);
  }

  public void printHunger() {
    String result = "HUNGER:   (";
    for (int i =0; i < hunger; i++) {
      result += "--";
    }
    for (int i = 0; i < 10-hunger; i++) {
      result += "  ";
    }
    result += ")";
    System.out.println(result);
  }

  public void printThirst() {
    String result = "THIRST:   (";
    for (int i =0; i < thirst; i++) {
      result += "--";
    }
    for (int i = 0; i < 10-thirst; i++) {
      result += "  ";
    }
    result += ")";
    System.out.println(result);
  }

  public void printCondition() {
    printFatigue();
    printHunger();
    printThirst();
  };

  public abstract void increaseTiredness();

  public abstract boolean checkTiredness();

  public void checkInventory() {
    if (inventory == null) {
      System.out.println("You have nothing in your inventory");
      return;
    }
    System.out.println("You are carrying:");
    for (int i = 0; i< inventory.length; i++){
      if (inventory[i] != null ) {
        System.out.println("   " + inventory[i].getName());
      }
    }
  }

  public int invSize() {
    int bagSize =0;
    for (int i = 0 ; i < inventory.length; i++){
      if (inventory[i] !=  null && !inventory.equals("")) {
        bagSize += 1;
      }
    }
    return bagSize;
  }

  public void take(Item item) {
    //Checks if item is already in inventory
    for (int i = 0; i < this.inventory.length; i++) {
      if (item.equals(inventory[i])) {
        System.out.println("You already have that item\n");
        return;
      }
    }
    for (int i = 0; i < this.inventory.length; i ++) {
      if ("".equals(inventory[i]) || inventory[i] == null) {
        inventory[i] = item;
        System.out.println(item.name + " put in sack\n");
        return;
      }
    }
    System.out.println("Your load is too heavy");
  }

  public void examine(Item item) {
    System.out.println("   " + item.getName());
    System.out.println(item.getDescription());

  }


}
