package sample;

public class Item {
  /* I have to create new item class to store item in inventory. My items have two variables */
  protected String name;
  protected String description;

  public Item(String name, String description){
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
