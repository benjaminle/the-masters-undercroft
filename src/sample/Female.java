package sample;

public class Female extends Character{
  public Female(String name, int age) {
    this.name = name;
    this.age = age;
    this.fatigue = 1;
    this.hunger = 2;
    this.thirst = 2;
    this.inventory = new Item[5];
  }

  @Override
  public void increaseTiredness(){
    /*Female character starts off less tired, hungry, and thirsty, but gets more tired as her inv.
      becomes heavier */
    double tirednessFactor = ((double)invSize() +8) /8;
    this.fatigue += 0.2*tirednessFactor;
    this.hunger += 0.2*tirednessFactor;
    this.thirst += 0.2;
  }

  public boolean checkTiredness(){
    if((this.fatigue > 10) || (this.hunger > 10) || (this.thirst > 10)) {
      return false;
    }
    return true;
  }



}
